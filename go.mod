module zlink.ltd/tcp_server

go 1.15

require (
	github.com/deckarep/golang-set v1.7.1
	github.com/fastly/go-utils v0.0.0-20180712184237-d95a45783239 // indirect
	github.com/go-akka/configuration v0.0.0-20200606091224-a002c0330665 // indirect
	github.com/gogap/config v0.0.0-20190801085516-e664631840ac // indirect
	github.com/gogap/logrus_mate v1.1.0
	github.com/guotie/gogb2312 v0.0.0-20160513031741-a052a05f3e7e
	github.com/jehiah/go-strftime v0.0.0-20171201141054-1d33003b3869 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible // indirect
	github.com/lestrrat-go/strftime v1.0.3 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/tebeka/strftime v0.1.5 // indirect
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
