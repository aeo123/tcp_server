package config

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	. "zlink.ltd/tcp_server/src/config/param"
)

type ConfigParam struct {
	Logger LogParam
	App    AppParam //应用参数
}

var Config ConfigParam

func init() {
	var b = viper.New()
	b.SetConfigType("yaml")
	b.SetConfigName("bootstrap")   // name of config file (without extension)
	b.AddConfigPath("./resources") // 配置文件路径，多次使用可以查找多个目录
	err := b.ReadInConfig()        // Find and read the config file
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
		return
	}
	b.SetDefault("application.mode", "debug")

	var mode string
	if os.Getenv("application.mode") == "" {
		mode = b.GetString("application.mode")
	} else {
		mode = os.Getenv("application.mode")
	}
	fmt.Println("System Mode:", mode)
	//加载应用配置
	var v = viper.New()
	v.SetConfigType("yaml")
	//选择环境文件
	if mode == "debug" {
		v.SetConfigName("application-dev") // name of config file (without extension)
	} else if mode == "prod" {
		v.SetConfigName("application-prod") // name of config file (without extension)
	} else {
		v.SetConfigName("application-test") // name of config file (without extension)
	}
	v.AddConfigPath("./resources") // 配置文件路径，多次使用可以查找多个目录
	err = v.ReadInConfig()         // Find and read the config file
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
		return
	}

	//加载配置
	Config.App.ReadConfig(b)
	Config.Logger.ReadConfig(b)
	logrus.Info("Config Read Over")
}
