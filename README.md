>更新2.2版本，消息和命令都不需要加\r\n了，支持中文
## 功能
1.ECHO回环模式
    连接服务器服务器回复'hello'，发什么回复什么
```
send-> 123
recv<- 123
```
2.组播
- 设备加入房间后发送消息会传到房间内另外的全部设备（不包括自己）
- 使用DTU和一个上位机就能自己调试DTU
```

//加入房间指令，每个设备只能加一个房间
CMD,GROUP,'name',['alias'] //name房间名称，alias设备别名（可不加）
示例：
send-> CMD,GROUP,123
recv<- Join Group:123

send-> CMD,GROUP,123,dtu  //别名dtu
recv<- Join Group:123


//离开房间，掉线自动离开
CMD,UNGROUP

//查询自己在哪个房间
CMD,QUERY

//查询房间里面全部设备
CMD,LIST

```
3.P2P
新建只有两个人的房间就能P2P

---

## 编译和安装
1. 安装go语言，升级到GO1.13版本以上，编译用的1.15
- go env -w GO111MODULE=on
- go env -w GOPROXY=https://goproxy.cn,direct
2. 安装依赖 go mod download
3. 编译windows：build_win.bat
4. 编译linux：build_linux.bat
5. 编译全部：build_all.bat
6. 启动程序需要和resource文件夹放一起；
修改端口在resources-> bootstrap.yaml-> port
   - windows启动：点击运行
   - linux启动：上传zip到服务器
   - 连接日志：logs文件夹
   ```
   //解压
   unzip tcp_server_linux_2.0.zip
   chmod 777 tcp_server_linux_2.0
   //运行
   ./tcp_server_linux_2.0
   //后台运行
   nohup ./tcp_server_linux_2.0 &
   ```

